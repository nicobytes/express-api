# For Debian/Ubuntu/Mint

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

apt-get install gitlab-runner
```


```bash
gitlab-runner \
    register -n \
    --name "Docker Runner" \
    --executor docker \
    --docker-image docker:latest \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --url https://gitlab.com/ \
    --registration-token VhGDAT9jtGuNFsKUAgvW \
    --tag-list docker-prod
```

```bash
gitlab-runner \
    register -n \
    --name "Shell Runner" \
    --executor shell \
    --url https://gitlab.com/ \
    --registration-token VhGDAT9jtGuNFsKUAgvW \
    --tag-list shell-prod
```

- Tanto el runner como la maquina deben tener el mismo executor
- Correr docker run en CI
- mover a docker?
- cambiar ejecutor ssh debería node? solo en la maquina?